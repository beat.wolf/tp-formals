package s03;

/**
 * <p>Description: Tests the DiagSynt class </p>
 * @author Stotzer
 * @version 1.0
 */
import java.util.Random;

import org.junit.Test;
import static org.junit.Assert.*;

public class MyDiagSyntTest {

  public static String generateCorrectWord(Random r, int maxNbr) {
    String word = "";
    int n1 = r.nextInt(maxNbr) + 1;
    int n2 = r.nextInt(maxNbr) + 1;
    for (int i = 0; i < n1; i++)
      word += 'i';
    word += '+';
    for (int i = 0; i < n2; i++)
      word += 'i';
    word += '=';
    for (int i = 0; i < n1 + n2; i++)
      word += 'i';
    return word;
  }

  public static void testWord(String word, boolean expectedResult) {
    boolean res = DiagSynt.isAccepted(word);
    
    if(expectedResult) {
        assertTrue( "'" + word + "' should be accepted", res);
    }else {
        assertFalse("'" + word + "' should be refused", res);
    }
  }

  public void testCorrectWords(Random r, int maxNbr, int nbrOfTest) {
    String word;

    // Following words should be accepted
    for (int i = 0; i < nbrOfTest; i++){
      word= generateCorrectWord(r, maxNbr);
      testWord(word, true);
    }
  }
  
  public void testWrongWords(Random r, int maxNbr, int nbrOfTest) {
    testWord("", false);
    testWord("=", false);
    testWord("i=i", false);
    
    String word;
    // Following words should be refused
    for (int i = 0; i < nbrOfTest; i++) {
      word = generateCorrectWord(r, maxNbr);
      if (r.nextBoolean())
        word = word.substring(1+r.nextInt(word.length()));
      else
        word = word.substring(0, 1 + r.nextInt(word.length() - 1));

      testWord(word, false);
    }
  }

  @Test
  public void test() {
      int maxNbr = 10;
      int nbrOfTest = 100;
      Random r = new Random();
      
      testWrongWords(r, maxNbr, nbrOfTest);
      testCorrectWords(r, maxNbr, nbrOfTest);
  }
  
}
