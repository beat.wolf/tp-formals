package s03;

import java.util.HashSet;
import java.util.Set;

public class FSM {
    private final int[][] transition; // copy of the transition table
    private final Set<Integer> acceptingStates;

    public FSM(int[][] transitions, // also gives n and m
            Set<Integer> acceptingStates) {
        this.transition = transitions;
        this.acceptingStates = acceptingStates;
    }

    public boolean accepts(int[] word) {
        int crtState = 0;
        for( int i = 0; i < word.length; i++) {
            crtState = transition[crtState][word[i]];
        }

        return acceptingStates.contains(crtState);
    }

    public int nStates() {
        return transition.length;
    }

    public int nSymbols() {
        return transition[0].length;
    }

    // -----------------------------------------------------------
    public static void main(String[] args) {
        if (args.length == 0)
            args = new String[] { "abbab", "baab", "bbabbb" };
        int[][] transitions = { { 1, 0 }, // <-- from state 0, with symbol
                                          // a,b,c...
                { 2, 0 }, // <-- from state 1, with symbol a,b,c...
                { 2, 2 } };
        Set<Integer> acceptingStates = new HashSet<>();
        acceptingStates.add(0);
        FSM autom = new FSM(transitions, acceptingStates);
        for (int i = 0; i < args.length; i++) {
            String letters = args[i];
            int[] word = new int[letters.length()];
            for (int j = 0; j < letters.length(); j++)
                word[j] = letters.charAt(j) - 'a';
            boolean res = autom.accepts(word);
            System.out.println(letters + " : " + res);
        }
    }
}
