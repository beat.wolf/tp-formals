package s04;

import java.util.HashMap;
import java.util.Map;

public class Interpreter {
  private static Lexer lexer;
  // TODO - A COMPLETER (ex. 3) 

  private static Map<String, Integer> variables = new HashMap<>();
  
  
  public static int evaluate(String e) throws ExprException {
    variables.clear();
    lexer = new Lexer(e);
    int res = parseExpr();
    // test if nothing follows the expression...
    if (lexer.crtSymbol().length() > 0)
      throw new ExprException("bad suffix : " + lexer.crtSymbol());
    return res;
  }

  private static int parseExpr() throws ExprException {
    int res = 0;
    
    boolean repeat = false;
    boolean add = true;
    do {
        repeat = false;
        int newValue = parseTerm();
        
        if(add) {
            res += newValue;
        }else {
            res -= newValue;
        }
        
        if(lexer.isPlus()) {
            lexer.goToNextSymbol();
            repeat = true;
            add = true;
        }else if(lexer.isMinus()) {
            lexer.goToNextSymbol();
            repeat = true;
            add = false;
        }
    }while(repeat);
    
    
    return res;
  }

  private static int parseTerm() throws ExprException {
    int res = 1;
    
    boolean repeat = false;
    boolean add = true;
    do {
        repeat = false;
        int newValue = parseFact();
        
        if(add) {
            res *= newValue;
        }else {
            res /= newValue;
        }
        
        if(lexer.isStar()) {
            lexer.goToNextSymbol();
            repeat = true;
            add = true;
        }else if(lexer.isSlash()) {
            lexer.goToNextSymbol();
            repeat = true;
            add = false;
        }
    }while(repeat);
    
    return res;
  }
  private static int parseFact() throws ExprException {
     int res = 0;

     if(lexer.isNumber()) { //handle raw values
         res = Integer.parseInt(lexer.crtSymbol());
         lexer.goToNextSymbol();
     }else if(lexer.isOpeningParenth()) {
         lexer.goToNextSymbol();
         
         res = parseExpr();

         if(lexer.isClosingParenth()) {
             lexer.goToNextSymbol();
         }else {
             throw new ExprException("Missing closing parenthesis, found "+ lexer.crtSymbol());
         }
         
         //Save variable if specified
         if(lexer.isIdent()) {
             String variableName = lexer.crtSymbol();
             variables.put(variableName, res);

             lexer.goToNextSymbol();
         }
     }else if(lexer.isIdent()) { //Handle function calls and variable usage
         String ident = lexer.crtSymbol();
         lexer.goToNextSymbol();
         
         if(lexer.isOpeningParenth()) {
             lexer.goToNextSymbol();
         }else {
             if(variables.containsKey(ident)) {
                 return variables.get(ident);
             }
             
             throw new ExprException("Missing opening parenthesis, found "+ lexer.crtSymbol());
         }

         
         res = applyFct(ident, parseExpr());
         
         if(lexer.isClosingParenth()) {
             lexer.goToNextSymbol();
         }else {
             throw new ExprException("Missing closing parenthesis, found "+ lexer.crtSymbol());
         }
     }else {
         throw new ExprException("No useable symbol");
     }
     
    return res;
  }

  private static int applyFct(String fctName, int arg) throws ExprException {
      switch(fctName) {
      case "abs":
          return Math.abs(arg);
      case "sqr":
          return arg * arg;
      case "cube":
          return arg * arg* arg;
      case "sqrt":
          if(arg < 0) {
              throw new ExprException("Can not calculate sqrt for negative number : " + arg);
          }
          return (int)Math.sqrt(arg);
      default:
          
          throw new ExprException("Unknown function " + fctName);
      }
  }
}
